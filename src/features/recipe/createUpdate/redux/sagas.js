import { takeEvery, put } from 'redux-saga/effects'
import {CREATE_RECIPE} from "./constants";
import {ADD_RECIPE} from "../../list/redux/constants";
import {ADD_MULTIPLE} from "../../ingredient/redux/constants";

function* createRecipe(action) {
  const ingredients = action.payload.ingredients.map(i => i.name);
  yield put({ type: ADD_MULTIPLE, payload: ingredients });

  yield put({ type: ADD_RECIPE, payload: action.payload })
}

export default function* saga() {
  yield takeEvery(CREATE_RECIPE, createRecipe)
}
