import {CLOSE_CU_MODAL, OPEN_CU_MODAL, CREATE_RECIPE} from "./constants";

export function closeCuModal() {
  return { type: CLOSE_CU_MODAL }
};

export function openCuModal(payload) {
  return { type: OPEN_CU_MODAL, payload }
};

export function createRecipe(payload) {
  return { type: CREATE_RECIPE, payload }
};
