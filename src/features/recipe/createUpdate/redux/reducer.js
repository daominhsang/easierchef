import {CLOSE_CU_MODAL, OPEN_CU_MODAL} from "./constants";

const initialState = {
  openDialog: false
};

function reducer(state = initialState, action) {
  switch (action.type) {
    case CLOSE_CU_MODAL:
      return Object.assign({}, state, {openDialog: false});
    case OPEN_CU_MODAL:
      return Object.assign({}, state, {openDialog: true});
    default:
      return state;
  }
}

export default reducer;
