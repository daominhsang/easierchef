import { createSelector } from 'reselect'
import {selectRecipe} from "../../redux/selectors";

export const selectCreateUpdateRecipe = createSelector(
  selectRecipe,
  (recipe) => recipe.createUpdateRecipe
);

export const selectOpenDialog = createSelector(
  selectCreateUpdateRecipe,
  (createUpdate) => createUpdate.openDialog
);

// export const selectRecipes = createSelector(
//   selectCreateUpdateRecipe,
//   (recipe) => recipe.data
// );
