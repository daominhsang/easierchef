import React from 'react';
import {Controller, useForm} from "react-hook-form";
import TextField from "@material-ui/core/TextField";
import {useDispatch} from "react-redux";
import Grid from "@material-ui/core/Grid";
import IngredientTable from "./IngredientTable";
import {isIngredientValid} from "./validators";
import SinglePictureInput from "../../../components/form-controls/SinglePictureInput";
import {closeCuModal, createRecipe} from "./redux/actions";

const CreateUpdateRecipe = ({saveBtnRef}) => {
  const dispatch = useDispatch();
  const {handleSubmit, control, errors, watch, reset} = useForm({
    defaultValues: {
      name: '',
      picture: '',
      steps: '',
      ingredients: []
    }
  });
  const values = watch();
  const onSubmit = data => {
    dispatch(createRecipe(data));
    dispatch(closeCuModal());

    reset({
      name: '',
      picture: '',
      steps: '',
      ingredients: []
    });
  };

  return (<form onSubmit={handleSubmit(onSubmit)} noValidate>
    <Grid container spacing={2}>
      <Grid item xs={12}>
        <Controller
          as={SinglePictureInput}
          control={control}
          name="picture"
          label="Dish Name"
          required
          rules={{
            validate: {
              isRequired: v => v.trim() === "" ? "Please upload picture" : null
            }
          }}
          error={Boolean(errors.picture)}
          helperText={errors.picture && errors.picture.message}
        />
      </Grid>
      <Grid item xs={12}>
        <Controller
          as={TextField}
          control={control}
          fullWidth
          variant="outlined"
          name="name"
          label="Dish Name"
          required
          rules={{
            validate: {
              isRequired: v => v.trim() === "" ? "Please input value" : null
            }
          }}
          error={Boolean(errors.name)}
          helperText={errors.name && errors.name.message}
          InputLabelProps={{shrink: values.name.length > 0}}
        />
      </Grid>
      <Grid item xs={12}>
        <Controller
          as={TextField}
          multiline={true}
          rows={7}
          control={control}
          fullWidth
          variant="outlined"
          name="steps"
          label="Steps to cook"
          required
          rules={{
            validate: {
              isRequired: v => v.trim() === "" ? "Please input value" : null
            }
          }}
          error={Boolean(errors.steps)}
          helperText={errors.steps && errors.steps.message}
          InputLabelProps={{shrink: values.steps.length > 0}}
        />
      </Grid>
      <Grid item xs={12}>
        <Controller
          as={IngredientTable}
          control={control}
          name="ingredients"
          required
          rules={{
            validate: {
              isRequired: v => (!v || (v && v.length === 0)) ? "Please input at least 1" : null,
              invalid: v => v.some(d => (!isIngredientValid(d))) ? "Make sure you have filled all values" : null
            }
          }}
          label="Ingredients"
          error={Boolean(errors.ingredients)}
          helperText={errors.ingredients && errors.ingredients.message}
        />
      </Grid>
    </Grid>

    <button style={{display: 'none'}} type="submit" ref={saveBtnRef}>Save</button>
  </form>)
};

export default CreateUpdateRecipe;
