import React, {useCallback, useEffect, useState} from 'react';
import TableContainer from "@material-ui/core/TableContainer";
import Paper from "@material-ui/core/Paper";
import Table from "@material-ui/core/Table";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import TableBody from "@material-ui/core/TableBody";
import IngredientRow from "./IngredientRow";
import {Box} from "@material-ui/core";
import FormLabel from "@material-ui/core/FormLabel";
import IconButton from "@material-ui/core/IconButton";
import AddIcon from "@material-ui/icons/Add";
import FormHelperText from "@material-ui/core/FormHelperText";
import {isIngredientValid} from "./validators";
import {randomGuid} from "../../../utils";

const IngredientTable = React.forwardRef((props, ref) => {
  const {name, onChange, value, error, helperText, label} = props;
  const [data, setData] = useState(value && Array.isArray(value) ? value : []);

  const onUpdate = useCallback((newData) => {
    const findData = data.findIndex(d => d.id === newData.id);
    if(findData > -1) {
      data[findData] = newData;
      onChange(data);
    }
  }, [data]);

  const onAddIngredient = useCallback(() => {
    const ingredientInvalid = data.some(d => {
      return !isIngredientValid(d)
    });

    if (!ingredientInvalid) {
      onChange([
        ...data,
        {id: randomGuid(), name: null, unit: '', quantity: '', open: true}
      ])
    }
  }, [data]);

  useEffect(() => {
    setData(value)
  }, [value.length]);

  return (<React.Fragment>
    <input type="hidden" name={name} value={value} ref={ref} />
    <Box display="flex" flexDirection="row" justifyContent="space-between" alignItems="center">
      <FormLabel>{label}</FormLabel>
      <IconButton size="small" onClick={onAddIngredient}>
        <AddIcon/>
      </IconButton>
    </Box>
    <Box style={{marginTop: 10}}>
      {error && <Box style={{marginBottom: 5}}>
        <FormHelperText error={true}>{helperText}</FormHelperText>
      </Box>}

      <TableContainer component={Paper}>
        <Table aria-label="collapsible table">
          <TableHead>
            <TableRow>
              <TableCell></TableCell>
              <TableCell>Name</TableCell>
              <TableCell>Quantity</TableCell>
              <TableCell>Unit</TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {data.map((dt) => (<IngredientRow onUpdate={onUpdate} key={dt.id} row={dt}/>))}
          </TableBody>
        </Table>
      </TableContainer>
    </Box>
  </React.Fragment>)
});

export default IngredientTable;
