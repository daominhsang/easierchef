export const isIngredientValid = (d) => d.name !== null && d.unit !== "" && d.quantity !== "";
