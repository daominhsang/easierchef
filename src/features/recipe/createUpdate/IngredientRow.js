import React, {useCallback, useEffect, useState} from 'react';
import {Controller, useForm} from "react-hook-form";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import TableRow from "@material-ui/core/TableRow";
import TableCell from "@material-ui/core/TableCell";
import IconButton from "@material-ui/core/IconButton";
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp';
import {makeStyles} from "@material-ui/core";
import Collapse from "@material-ui/core/Collapse";
import Grid from "@material-ui/core/Grid";
import Box from "@material-ui/core/Box";
import Autocomplete from "../../../components/form-controls/Autocomplete";
import {useSelector} from "react-redux";
import {selectIngredients} from "../ingredient/redux/selectors";

const useRowStyles = makeStyles({
  root: {
    '& > *': {
      borderBottom: 'unset',
    },
  },
});

const IngredientRow = ({row, onUpdate}) => {
  const classes = useRowStyles();
  const [open, setOpen] = useState(row.open);
  const {control, errors, watch, reset, trigger, getValues} = useForm({
    defaultValues: row
  });
  const values = watch();
  const ingredients = useSelector(selectIngredients);

  const onSubmit = useCallback(() => {
    trigger().then((valid) => {
      if (onUpdate && valid) {
        onUpdate(getValues());
        setOpen(false);
      }
    });
  }, [onUpdate]);

  const onCancel = useCallback(() => {
    setOpen(false);
  }, []);

  useEffect(() => {
    if (!open) {
      reset(row)
    }
  }, [open]);

  return (<React.Fragment>
    <TableRow className={classes.root}>
      <TableCell>
        <IconButton aria-label="expand row" size="small" onClick={() => setOpen(!open)}>
          {open ? <KeyboardArrowUpIcon/> : <KeyboardArrowDownIcon/>}
        </IconButton>
      </TableCell>
      <TableCell component="th" scope="row">
        {row.name ? row.name.title : ''}
      </TableCell>
      <TableCell>{row.quantity}</TableCell>
      <TableCell>{row.unit}</TableCell>
    </TableRow>
    <TableRow>
      <TableCell style={{paddingBottom: 0, paddingTop: 0}} colSpan={4}>
        <Collapse in={open} timeout="auto" unmountOnExit style={{marginBottom: 10}}>
          <Grid container spacing={2}>
            <Controller
              render={({name, value}) => <input type="hidden" name={name} value={value}/>}
              control={control}
              name="id"
            />
            <input type="hidden" name="id"/>

            <Grid item container spacing={2} xs={12}>
              <Grid item xs={4}>
                <Controller
                  as={Autocomplete}
                  control={control}
                  name="name"
                  label="Name"
                  rules={{
                    validate: {
                      isRequired: v => v === null ? "Please input value" : null
                    }
                  }}
                  error={Boolean(errors.name)}
                  helperText={errors.name && errors.name.message}
                  options={ingredients}
                />
              </Grid>
              <Grid item xs={4}>
                <Controller
                  as={TextField}
                  control={control}
                  fullWidth
                  variant="outlined"
                  name="quantity"
                  label="Quantity"
                  required
                  rules={{
                    validate: {
                      isRequired: v => v.trim() === "" ? "Please input value" : null
                    }
                  }}
                  error={Boolean(errors.quantity)}
                  helperText={errors.quantity && errors.quantity.message}
                  InputLabelProps={{shrink: values.quantity.length > 0}}
                />
              </Grid>
              <Grid item xs={4}>
                <Controller
                  as={TextField}
                  control={control}
                  fullWidth
                  variant="outlined"
                  name="unit"
                  label="Unit"
                  required
                  rules={{
                    validate: {
                      isRequired: v => v.trim() === "" ? "Please input value" : null
                    }
                  }}
                  error={Boolean(errors.unit)}
                  helperText={errors.unit && errors.unit.message}
                  InputLabelProps={{shrink: values.unit.length > 0}}
                />
              </Grid>
            </Grid>
            <Grid item xs={12}>
              <Box display="flex" flexDirection="row">
                <Box m={1}>
                  <Button variant="contained" onClick={onCancel}>Cancel</Button>
                </Box>
                <Box m={1}>
                  <Button variant="contained" color="primary" onClick={onSubmit}>
                    Save
                  </Button>
                </Box>
              </Box>
            </Grid>
          </Grid>
        </Collapse>
      </TableCell>
    </TableRow>
  </React.Fragment>)
};

export default IngredientRow;
