import {combineReducers} from "redux";
import recipeList from '../list/redux/reducer';
import createUpdateRecipe from '../createUpdate/redux/reducer';
import ingredientRecipe from '../ingredient/redux/reducer';

export default combineReducers({
  recipeList,
  createUpdateRecipe,
  ingredientRecipe
})
