import {all} from "redux-saga/effects";

import listSaga from '../list/redux/sagas'
import createUpdateSaga from '../createUpdate/redux/sagas'
import ingredientUpdateSaga from '../ingredient/redux/sagas'

export default function* saga() {
  yield all([
    listSaga(),
    createUpdateSaga(),
    ingredientUpdateSaga()
  ])
}
