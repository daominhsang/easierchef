import React, {useCallback, useRef} from 'react';
import RecipeItem from "./RecipeItem";
import Grid from "@material-ui/core/Grid";
import Box from "@material-ui/core/Box";
import {useDispatch, useSelector} from "react-redux";
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import DialogActions from "@material-ui/core/DialogActions";
import Button from "@material-ui/core/Button";
import {selectRecipes} from "./redux/selectors";
import {selectOpenDialog} from "../createUpdate/redux/selectors";
import {closeCuModal} from "../createUpdate/redux/actions";
import useMediaQuery from "@material-ui/core/useMediaQuery";
import useTheme from "@material-ui/core/styles/useTheme";
import CreateUpdateRecipe from "../createUpdate/CreateUpdateRecipe";
import {makeStyles} from "@material-ui/core";

const useStyles = makeStyles((fullScreen) => ({
  paper: {minWidth: fullScreen ? 0 : "600px"},
  noData: {
    height: '100%',
    color: '#cccccc',
  }
}));

const RecipeList = () => {
  const theme = useTheme();
  const fullScreen = useMediaQuery(theme.breakpoints.down('sm'));
  const classes = useStyles(fullScreen);
  const dispatch = useDispatch();
  const data = useSelector(selectRecipes);
  const openDialog = useSelector(selectOpenDialog);
  const submitBtnRef = useRef();
  const cancelBtnRef = useRef();

  const handleClose = useCallback(() => {
    dispatch(closeCuModal())
  }, []);

  const onSubmit = useCallback(() => {
    if (submitBtnRef && submitBtnRef.current) {
      submitBtnRef.current.click()
    }
  }, []);

  return (<React.Fragment>
      {data.length === 0 && <Box className={classes.noData} display="flex" alignItems="center" justifyContent="center">No recipe found</Box>}
      <Box>
        <Grid container spacing={4}>
          {data.map((dt) => (<Grid key={dt.id} item xs={12} sm={6} md={4}>
            <RecipeItem item={dt}/>
          </Grid>))}
        </Grid>
      </Box>
      <Dialog classes={{paper: classes.paper}} fullScreen={fullScreen} open={openDialog} onClose={handleClose}
              aria-labelledby="form-dialog-title">
        <DialogTitle id="form-dialog-title">New Recipe</DialogTitle>
        <DialogContent>
          <CreateUpdateRecipe saveBtnRef={submitBtnRef} cancelBtnRef={cancelBtnRef}/>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose} color="primary">
            Cancel
          </Button>
          <Button onClick={onSubmit} color="primary">
            Submit
          </Button>
        </DialogActions>
      </Dialog>
    </React.Fragment>
  )
};

export default RecipeList;
