import {ADD_RECIPE, SET_FILTER} from "./constants";

export function addRecipe(payload) {
  return { type: ADD_RECIPE, payload }
}

export function setFilter(payload) {
  return { type: SET_FILTER, payload }
}
