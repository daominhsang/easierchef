import { createSelector } from 'reselect'
import {selectRecipe} from "../../redux/selectors";

export const selectRecipeList = createSelector(
  selectRecipe,
  (recipe) => recipe.recipeList
);

export const selectData = createSelector(
  selectRecipeList,
  (state) => state.data
);

export const selectFilter = createSelector(
  selectRecipeList,
  (state) => state.filter
);

export const selectRecipes = createSelector(
  selectData,
  selectFilter,
  (data, filter) => {
    let result = data;
    if(filter.length) {
      result = result.filter(r => r.ingredients.some(i => filter.includes(i.name.title)))
    }
    return result;
  }
);
