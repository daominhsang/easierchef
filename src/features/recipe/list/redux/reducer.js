import {ADD_RECIPE, SET_FILTER} from "./constants";
import {randomGuid} from "../../../../utils";

const initialState = {
  data: [],
  filter: []
};

function reducer(state = initialState, action) {
  switch (action.type) {
    case ADD_RECIPE:
      return Object.assign({}, state, {
        data: [
          ...state.data,
          {id: randomGuid(), ...action.payload}
        ]
      });
    case SET_FILTER:
      return Object.assign({}, state, {
        filter: action.payload
      });
    default:
      return state;
  }
}

export default reducer;
