import {ADD_MULTIPLE} from "./constants";

export function addMultipleIngredients(payload) {
  return { type: ADD_MULTIPLE, payload }
}
