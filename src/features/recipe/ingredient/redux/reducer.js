import {ADD_MULTIPLE} from "./constants";

const initialState = {
  data: []
};

function reducer(state = initialState, action) {
  switch (action.type) {
    case ADD_MULTIPLE:
      const newData = action.payload.filter(i => !state.data.find(t => t.title === i.title));
      if(newData.length > 0) {
        return Object.assign({}, state, {
          data: [
            ...state.data,
            ...newData
          ]
        });
      }
      return state;
    default:
      return state;
  }
}

export default reducer;
