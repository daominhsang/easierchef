import { createSelector } from 'reselect'
import {selectRecipe} from "../../redux/selectors";

export const selectState = createSelector(
  selectRecipe,
  (state) => state.ingredientRecipe
);

export const selectIngredients = createSelector(
  selectState,
  (state) => state.data
);
