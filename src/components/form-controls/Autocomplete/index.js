import React from 'react';
import TextField from '@material-ui/core/TextField';
import AC, { createFilterOptions } from '@material-ui/lab/Autocomplete';

const filter = createFilterOptions();

const Autocomplete = React.forwardRef((props, ref) => {
  const {value, onChange, onBlur, options, ...rest} = props;
  const [currentValue, setCurrentValue] = React.useState(value);

  const handleChange = (newValue) => {
    setCurrentValue(newValue);
    onChange(newValue)
  };

  return (<AC
    value={currentValue}
    onChange={(event, newValue) => {
      if (typeof newValue === 'string') {
        handleChange({
          title: newValue,
        });
      } else if (newValue && newValue.inputValue) {
        // Create a new value from the user input
        handleChange({
          title: newValue.inputValue,
        });
      } else {
        handleChange(newValue);
      }
    }}
    filterOptions={(options, params) => {
      const filtered = filter(options, params);

      // Suggest the creation of a new value
      if (params.inputValue !== '') {
        filtered.push({
          inputValue: params.inputValue,
          title: `Add "${params.inputValue}"`,
        });
      }

      return filtered;
    }}
    selectOnFocus
    clearOnBlur
    handleHomeEndKeys
    options={options}
    getOptionLabel={(option) => {
      // Value selected with enter, right from the input
      if (typeof option === 'string') {
        return option;
      }
      // Add "xxx" option created dynamically
      if (option.inputValue) {
        return option.inputValue;
      }
      // Regular option
      return option.title;
    }}
    renderOption={(option) => option.title}
    // style={{width: 300}}
    freeSolo
    renderInput={(params) => (
      <TextField {...params} variant="outlined" {...rest}/>
    )}
  />)
});

export default Autocomplete;
