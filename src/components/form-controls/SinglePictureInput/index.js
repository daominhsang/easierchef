import React, {useCallback, useRef} from 'react';
import Box from "@material-ui/core/Box";
import {makeStyles} from "@material-ui/core";
import Button from "@material-ui/core/Button";
import FormLabel from "@material-ui/core/FormLabel";
import FormHelperText from "@material-ui/core/FormHelperText";

const useStyles = makeStyles(() => ({
  container: {},
  imageContainer: {
    width: 100,
    height: 100,
    borderWidth: 1,
    borderColor: '#cccccc',
    borderStyle: 'dashed',
    marginRight: 20,
    backgroundColor: '#f1f1f1',
    textAlign: 'center'
  },
  image: {
    objectFit: 'contain',
    maxWidth: '100%',
    maxHeight: '100%'
  }
}));

const SinglePictureInput = React.forwardRef(({name, value, onChange, error, helperText, label}, ref) => {
  const classes = useStyles();
  const inputFileRef = useRef();

  const onRemove = useCallback(() => {
    onChange('');
  }, []);

  const onSelect = useCallback(() => {
    inputFileRef.current.click();
  }, []);

  const onFileChange = useCallback((e) => {
    if(e.target.files[0]) {
      const file = e.target.files[0];
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => {
        onChange(reader.result);
      };
      reader.onerror = error => {
        console.error(error);
      };
    }
  }, []);

  return (<React.Fragment>
    <input type="hidden" name={name} value={value} ref={ref}/>
    {/*<FormLabel>{label}</FormLabel>*/}
    <Box display="flex" flexDirection="row" className={classes.container}>
      <Box className={classes.imageContainer}>
        {value && <img src={value} className={classes.image}/>}
      </Box>
      <Box flexGrow={1} display="flex" alignItems="flex-start" justifyContent="flex-end" flexDirection="column">
        {value && <Button variant="contained" color="secondary" onClick={onRemove} style={{marginBottom: 5}}>Remove</Button>}
        <Button variant="contained" color="primary"  onClick={onSelect}>{
          value ? 'Change Picture' : 'Upload Picture'
        }</Button>
        <input ref={inputFileRef} type="file" style={{display: 'none'}} onChange={onFileChange} />
      </Box>
    </Box>
    {error && <Box style={{marginBottom: 5}}>
      <FormHelperText error={true}>{helperText}</FormHelperText>
    </Box>}
  </React.Fragment>)
});

export default SinglePictureInput;
