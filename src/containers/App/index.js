import React, {useCallback} from 'react';
import CssBaseline from "@material-ui/core/CssBaseline";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import Button from "@material-ui/core/Button";
import Container from "@material-ui/core/Container";
import Box from "@material-ui/core/Box";
import Link from "@material-ui/core/Link";
import makeStyles from "@material-ui/core/styles/makeStyles";
import {useDispatch} from "react-redux";
import useMediaQuery from "@material-ui/core/useMediaQuery";
import {useTheme} from "@material-ui/core";
import AddIcon from "@material-ui/icons/Add";
import {openCuModal} from "../../features/recipe/createUpdate/redux/actions";
import Filter from "./Filter";


const useStyles = makeStyles((theme) => ({
    '@global': {
      ul: {
        margin: 0,
        padding: 0,
        listStyle: 'none',
      },
    },
    appBar: {
      borderBottom: `1px solid ${theme.palette.divider}`,
    },
    toolbar: {
      flexWrap: 'wrap',
    },
    toolbarTitle: {},
    filter: {
      flexGrow: 1,
    },
    link: {
      margin: theme.spacing(1, 1.5),
    },
    main: {
      display: 'flex',
      flex: 1,
    },
    content: {
      height: '100%',
      width: '100%'
    }
  }
));

const App = ({children}) => {
  const theme = useTheme();
  const classes = useStyles();
  const dispatch = useDispatch();

  const isXs = useMediaQuery(theme.breakpoints.down('xs'));

  const openRecipeCuModal = useCallback(() => {
    dispatch(openCuModal());
  }, []);

  return (
    <React.Fragment>
      <CssBaseline/>
      <AppBar position="static" color="default" elevation={0} className={classes.appBar}>
        <Toolbar className={classes.toolbar}>
          <Typography variant="h6" color="inherit" noWrap>
            EasierChef
          </Typography>
          <Box className={classes.filter} display="flex" alignItems={"center"} justifyContent={"center"}>
            <Filter/>
          </Box>
          <Button color="primary" variant="outlined" className={classes.link} onClick={openRecipeCuModal}>
            {!isXs && 'Create Recipe'}
            {isXs && <AddIcon />}
          </Button>
        </Toolbar>
      </AppBar>
      <Container component="main" maxWidth={false} className={classes.main}>
        <Box m={1} className={classes.content}>
          {children}
        </Box>
      </Container>
      {/* Footer */}
      <Container maxWidth="md" component="footer" className={classes.footer}>
        <Box>
          <Typography variant="body2" color="textSecondary" align="center">
            {'Copyright © '}
            <Link color="inherit" href="#">
              Shawn Dao
            </Link>{' '}
            {new Date().getFullYear()}
            {'.'}
          </Typography>
        </Box>
      </Container>
      {/* End footer */}
    </React.Fragment>
  )
};


export default App;
