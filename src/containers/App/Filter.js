import React from 'react';
import Select from "@material-ui/core/Select";
import Input from "@material-ui/core/Input";
import MenuItem from "@material-ui/core/MenuItem";
import Checkbox from "@material-ui/core/Checkbox";
import ListItemText from "@material-ui/core/ListItemText";
import {useDispatch, useSelector} from "react-redux";
import {selectIngredients} from "../../features/recipe/ingredient/redux/selectors";
import {makeStyles} from "@material-ui/core";
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import {setFilter} from "../../features/recipe/list/redux/actions";

const ITEM_HEIGHT = 48;
const ITEM_PADDING_TOP = 8;
const MenuProps = {
  PaperProps: {
    style: {
      maxHeight: ITEM_HEIGHT * 4.5 + ITEM_PADDING_TOP,
      width: 250,
    },
  },
};

const useStyles = makeStyles((theme) => ({
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
    maxWidth: 300,
  }
}));

const Filter = () => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const ingredients = useSelector(selectIngredients);
  const [selectedIngredients, setSelectedIngredients] = React.useState([]);

  const handleChange = (event) => {
    setSelectedIngredients(event.target.value);
    dispatch(setFilter(event.target.value));
  };

  if(ingredients.length === 0) return null;

  return (<FormControl className={classes.formControl}>
    <Select
      multiple
      value={selectedIngredients}
      onChange={handleChange}
      input={<Input/>}
      renderValue={(selected) => selected.join(', ')}
      MenuProps={MenuProps}
    >
      {ingredients.map((name) => (
        <MenuItem key={name.title} value={name.title}>
          <Checkbox checked={selectedIngredients.indexOf(name.title) > -1}/>
          <ListItemText primary={name.title}/>
        </MenuItem>
      ))}
    </Select>
  </FormControl>)
};

export default Filter;
