import React from "react";
import {Provider} from "react-redux";
import {PersistGate} from 'redux-persist/integration/react'
import configureStore from "./store";
import AppContainer from "./containers/App";
import RecipeList from "./features/recipe/list/RecipeList";
import "./App.css"
const {store, persistor} = configureStore();

function App() {
  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={persistor}>
        <AppContainer>
          <RecipeList/>
        </AppContainer>
      </PersistGate>
    </Provider>
  );
}

export default App;
