import { createStore, applyMiddleware } from "redux";
import logger from 'redux-logger';
import { persistStore, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';
import createSagaMiddleware from 'redux-saga';
import rootReducer from "./rootReducer";
import rootSagas from './rootSagas'

const persistConfig = {
  key: 'root',
  storage,
};

const persistedReducer = persistReducer(persistConfig, rootReducer);

const sagaMiddleware = createSagaMiddleware();

let store = createStore(persistedReducer, applyMiddleware(logger, sagaMiddleware));

sagaMiddleware.run(rootSagas);

export default () => {
  let persistor = persistStore(store);
  return { store, persistor }
}
