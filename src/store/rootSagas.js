import {all} from "redux-saga/effects";

import recipeSaga from "../features/recipe/redux/sagas"

export default function* rootSaga() {
  yield all([
    recipeSaga()
  ])
}
