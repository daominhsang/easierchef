import {combineReducers} from "redux";
import recipeReducer from '../features/recipe/redux/reducer';

export default combineReducers({
  recipe: recipeReducer
});
